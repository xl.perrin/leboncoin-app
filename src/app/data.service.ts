import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable()
export class DataService {
  listProducts = [];

  product =  {
    id : 0,
    title : "",
    image : "",
    price : "",
    description : "",
    date : ""
  }

  productSubject = new Subject<any>();
  listProductsSubject = new Subject<[]>();

  constructor() { 
    this.emit();
  }

  emit = () => {
    this.productSubject.next(this.product);
  }

  emitList = () => {
    this.listProductsSubject.next(this.getProduct());
  }

  setProduct = (product) => {
    let liste = this.getProduct();
    liste.push(product)
    localStorage.setItem('listeProducts', JSON.stringify(liste));
    this.emitList();
  }

  getProduct = () => {
    return (JSON.parse(localStorage.getItem('listeProducts')) != null) ?JSON.parse(localStorage.getItem('listeProducts')) :[];
  }

  getMaxId = () => {
    let max = 0;
    if (this.getProduct().length > 0) {
      max = this.getProduct()[0].id;
      for (let i = 1; i < this.getProduct().length; i++) {
        if (max < this.getProduct()[i].id) {
          max = this.getProduct()[i].id
        }
      }
    }
    return max;
  }
}
