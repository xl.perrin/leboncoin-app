import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  product;
  constructor(private mesData : DataService, private mesRoutes: Router) { }

  ngOnInit() {
    this.mesData.productSubject.subscribe((newProduct) => {
      this.product = newProduct;
    })

    this.product = this.mesData.product;
  }

  change = (event) => {
    this.mesData.product[event.target.getAttribute('name')] = event.target.value;
  }

  confirm = () => {
    if (this.mesData.product.id == 0) {
      this.mesData.product.id = this.mesData.getMaxId() + 1;
      this.mesData.setProduct({ ...this.mesData.product });
     
    }
    this.mesRoutes.navigate([""])
  }
}
