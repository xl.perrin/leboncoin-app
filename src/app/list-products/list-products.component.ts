import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {
  listProducts = [];

  constructor(private mesData : DataService) { }

  ngOnInit() {
    this.mesData.listProductsSubject.subscribe((newList)=> {
      this.listProducts = newList;
    })
    this.listProducts = this.mesData.getProduct();
  }

}
